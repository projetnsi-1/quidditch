from random import randint 
# -*- coding: utf-8 -*-
liste_persos = []
with open("Characters.csv", mode='r', encoding='utf-8') as f:
    lines = f.readlines()
    key_line = lines[0].strip()
    keys = key_line.split(";")
    for line in lines[1:]:
        line = line.strip()
        values = line.split(';')
        dico = {}
        for i in range(len(keys)):
            if keys[i] == 'Name':
                dico[keys[i]] = values[i]
            if keys[i] == 'House':
                dico[keys[i]] = values[i]
        liste_persos.append(dico)
        
        
maisons = ["Gryffindor", "Slytherin", "Ravenclaw", 
           "Hufflepuff"]


resultat = []

for j in range(140):
        liste_persos[j]['Score'] = 2
dico_points = {}

def deux_matchs():
    """
    Elle détermine qui va affronter qui et fais jouer deux matchs, 
    c'est-à-dire une partie 
    
    renvoie: tuple (resultats des deux parties)    
    """
    equipe1 = maisons[randint(0, 3)]
    equipe2 = maisons[randint(0, 3)]
    equipe3 = maisons[randint(0, 3)]
    equipe4 = maisons[randint(0, 3)]
    while equipe2 == equipe1 or equipe3 ==  equipe1 or equipe4 == equipe1 or\
        equipe2 == equipe3 or equipe2 == equipe4 or equipe3 == equipe4:
            
        equipe2 = maisons[randint(0, 3)]
        equipe3 = maisons[randint(0, 3)]
        equipe4 = maisons[randint(0, 3)]
   
    
    def partie(equipe_1, equipe_2):

        scores = {equipe_1: 0, equipe_2: 0}
        temps = 0
        fin = False
        while fin == False:
            points1 = 0
            points2 = 0
            but = randint(1, 3)
            if but == 3:
                points1 += 10
            elif but == 1:
                points2 += 10
            vifdor = randint(1, 50)
            if vifdor == 50:
                points1 += 150
                fin = True
            elif vifdor == 1:
                points2 += 150
                fin = True
            scores[equipe_1] += points1
            scores[equipe_2] += points2
            temps += 5        
        if scores[equipe_1] > scores[equipe_2]:
            gagnant = equipe_1
        else:
            gagnant = equipe_2
        rendu = (gagnant, scores, temps)
        return rendu
        
    return partie(equipe1, equipe2), partie(equipe3, equipe4)
      


for i in range(10):
    resultat.append(deux_matchs())
    gagnant1 = resultat[i][0][0]
    gagnant2 = resultat[i][1][0]
    score_gagnant1 = resultat[i][0][1][gagnant1]
    score_gagnant2 = resultat[i][1][1][gagnant2]
    for j in range(140):
        
        if liste_persos[j]['House'] not in maisons:
           liste_persos[j]['House'] = maisons[randint(0, 3)]
        
        if liste_persos[j]['Score'] > 0:
            liste_persos[j]['Score'] -= 1 
            pari = 10 * randint(15, 70)
            if gagnant1 == liste_persos[j]['House']:
                liste_persos[j]['Score'] += 5
                if score_gagnant1 + 30 > pari > score_gagnant1 - 30:
                    liste_persos[j]['Score'] += 30
            if gagnant2 == liste_persos[j]['House']:
                liste_persos[j]['Score'] += 5
                if score_gagnant2 + 30 > pari > score_gagnant2 - 30:
                    liste_persos[j]['Score'] += 30
        dico_points[liste_persos[j]['Name']] = liste_persos[j]['Score']


print("Résultats des parties(gagnant, scores des équipes, temps en minutes)")
for i in range(10):
    print(f"Partie {i+1}")
    print(resultat[i][0])
    print(resultat[i][1])
    print()
    
    

classement = list(sorted(dico_points.items(), key=lambda t: t[1], reverse = True))

print("Voici le classement des personnages :")
print()
for i in range(len(classement)):
    print(f'{classement[i][0]} a obtenu un score total de {classement[i][1]}.')
print()
    
somme = 0
for j in range(140):
    somme += liste_persos[j]['Score']
print(f"La moyenne des scores est de : {round(somme / 140, 2)}")
print()


print("Le(s) gagnant(s) sont :")
compteur = 0
while classement[0][1] == classement[compteur][1]:
    print(classement[compteur][0])
    compteur += 1
