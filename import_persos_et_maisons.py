# -*- coding: utf-8 -*-
"""
Created on Fri Nov 19 16:54:41 2021

@author: Agathe TANCRET
"""
"""
Ce code permet d'importer et de creer un dictionnaire comportant 
les noms et la maison des personnages.

"""

liste_persos = []
with open("U:/Documents/nSi/nOte boOks mOdiFieS/Characters(1).csv", mode='r', encoding='utf-8') as f:
    """
    A la place du lien au dessus, il faut mettre le lien de l'emplacement ou
    ce trouve le dossier Characters.csv
    
    """
    lines = f.readlines()
    key_line = lines[0].strip()
    keys = key_line.split(";")
    for line in lines[1:]:
        line = line.strip()
        values = line.split(';')
        dico = {}
        for i in range(len(keys)):
            if keys[i] == 'Name':
                dico[keys[i]] = values[i]
            if keys[i] == 'House':
                dico[keys[i]] = values[i]
        liste_persos.append(dico)
    
print(liste_persos)
