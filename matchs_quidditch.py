from random import randint 
# -*- coding: utf-8 -*-
"""
Created on Sun Nov 14 11:08:41 2021

@author: clemence
goal: générer des matchs de quidditch
"""
maisons = ["Griffondor", "Serpentard", "Serdaigle", 
           "Poufsouffle"]
victoires_equipe1 = 0
victoires_equipe2 = 0
victoires_equipe3 = 0
victoires_equipe4 = 0


def deux_matchs():
    """
    dit qui va affronter qui dans les matchs 1 et 2

    Returns
    -------
    match1 : liste
        deux maisons.
    match2 : liste
        les deux autres maisons
    """
    equipe1 = maisons[randint(0, 3)]
    equipe2 = maisons[randint(0, 3)]
    equipe3 = maisons[randint(0, 3)]
    equipe4 = maisons[randint(0, 3)]
    while not equipe2 != equipe1 != equipe3 !=  equipe4:
        equipe1 = maisons[randint(0, 3)]
        equipe2 = maisons[randint(0, 3)]
        equipe3 = maisons[randint(0, 3)]
        equipe4 = maisons[randint(0, 3)]
    match1 =[equipe1, equipe2]
    match2 = [equipe3, equipe4]

deux_matchs()

def partie():
    scores = {equipe1: 0, equipe2: 0, equipe3: 0, equipe4: 0}
    temps = 0
    fin = False
    while fin:
            
        def tranche(x):
            points = 0
            but = randint(1, 3)
            if but == 3:
                points += 10
            vifdor = randint(1, 50)
            if vifdor == 50:
                points += 150
            fin = True
            scores[x] += points
                
        tranche(equipe1)
        tranche(equipe2)
        tranche(equipe3)
        tranche(equipe4)
        temps += 5
            
partie()

if scores[equipe1] > scores[equipe2]:
    victoires_equipe1 += 1
    vic_1 = True
    vic_2 = False
else:
    victoires_equipe2 += 1
    vic_2 = True
    vic_1 = False
if scores[equipe3] > scores[equipe4]:
    victoires_equipe3 += 1
    vic_3 = True
    vic_4 = False
else:
    victoires_equipe4 += 1
    vic_4 = True
    vic_3 = False
