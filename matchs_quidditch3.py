from random import randint 
# -*- coding: utf-8 -*-
"""
Created on Sun Nov 14 11:08:41 2021

@author: clemence
goal: générer des matchs de quidditch
"""
maisons = ["Griffondor", "Serpentard", "Serdaigle", 
           "Poufsouffle"]
victoires_griffondor = 0
victoires_serpentard = 0
victoires_serdaigle = 0
victoires_poufsouffle = 0

resultat = []


def deux_matchs():
    """
    Elle détermine qui va affronter qui et fais jouer deux matchs, 
    c'est-à-dire une partie 
    
    renvoie: tuple (resultats des deux parties)    
    """
    equipe1 = maisons[randint(0, 3)]
    equipe2 = maisons[randint(0, 3)]
    equipe3 = maisons[randint(0, 3)]
    equipe4 = maisons[randint(0, 3)]
    while equipe2 == equipe1 or equipe3 ==  equipe1 or equipe4 == equipe1 or equipe2 == equipe3 or equipe2 == equipe4 or equipe3 == equipe4:
        equipe2 = maisons[randint(0, 3)]
        equipe3 = maisons[randint(0, 3)]
        equipe4 = maisons[randint(0, 3)]
   
    
    def partie(equipe_1, equipe_2):Rend
    """
    Elle fait jouer les matchs

    parametres: chaines de caracteres (nom des equipes)
    renvoie: tuple (gagnant, score temps)
    """
        scores = {equipe_1: 0, equipe_2: 0}
        temps = 0
        fin = False
        while fin == False:
            points1 = 0
            points2 = 0
            but = randint(1, 3)
            if but == 3:
                points1 += 10
            elif but == 1:
                points2 += 10
            vifdor = randint(1, 50)
            if vifdor == 50:
                points1 += 150
                fin = True
            elif vifdor == 1:
                points2 += 150
                fin = True
            scores[equipe_1] += points1
            scores[equipe_2] += points2
            temps += 5        
        if scores[equipe_1] > scores[equipe_2]:
            gagnant = equipe_1
        else:
            gagnant = equipe_2
        rendu = (gagnant, scores, temps)
        return rendu
        
    return partie(equipe1, equipe2), partie(equipe3, equipe4)
      


for i in range(10):
    resultat.append(deux_matchs())

    if resultat[i][0][0] == "Griffondor" or resultat[i][1][0] == "Griffondor":
        victoires_griffondor += 1
    elif resultat[i][0][0] == "Serpentard" or resultat[i][1][0] == "Serpentard":
        victoires_serpentard += 1
    elif resultat[i][0][0] == "Serdaigle" or resultat[i][1][0] == "Serdaigle":
        victoires_serdaigle += 1
    elif resultat[i][0][0] == "Poufsouffle" or resultat[i][1][0] == "Poufsouffle":
        victoires_poufsouffle += 1



print(resultat)
