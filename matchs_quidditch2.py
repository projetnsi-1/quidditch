from random import randint 
# -*- coding: utf-8 -*-
"""
Created on Sun Nov 14 11:08:41 2021

@author: clemence
goal: générer des matchs de quidditch
"""
maisons = ["Griffondor", "Serpentard", "Serdaigle", 
           "Poufsouffle"]
victoires_equipe1 = 0
victoires_equipe2 = 0
victoires_equipe3 = 0
victoires_equipe4 = 0


def deux_matchs():
    """
    dit qui va affronter qui dans les matchs 1 et 2

    Returns
    -------
    match1 : liste
        deux maisons.
    match2 : liste
        les deux autres maisons
    """
    equipe1 = maisons[randint(0, 3)]
    equipe2 = maisons[randint(0, 3)]
    equipe3 = maisons[randint(0, 3)]
    equipe4 = maisons[randint(0, 3)]
    while not equipe2 != equipe1 != equipe3 !=  equipe4:
        equipe1 = maisons[randint(0, 3)]
        equipe2 = maisons[randint(0, 3)]
        equipe3 = maisons[randint(0, 3)]
        equipe4 = maisons[randint(0, 3)]
    match1 = [equipe1, equipe2]
    match2 = [equipe3, equipe4]
    
    def partie1():
        scores = {equipe1: 0, equipe2: 0}
        temps = 0
        fin = False
        while fin:
            points1 = 0
            points2 = 0
            but = randint(1, 3)
            if but == 3:
                points1 += 10
            elif but == 1:
                points2 += 10
            vifdor = randint(1, 50)
            if vifdor == 50:
                points1 += 150
                fin = True
            elif vifdor == 1:
                points2 += 150
                fin = True
            scores[equipe1] += points1
            scores[equipe2] += points2
            temps += 5
        resultats1 = tuple(scores)
        
        if resultats1[0] > resultats1[1]:
            vic_1 = True
            vic_2 = False
        else:
            vic_2 = True
            vic_1 = False
        
    def partie2():
        scores = {equipe3: 0, equipe4: 0}
        temps = 0
        fin = False
        while fin:
            points3 = 0
            points4 = 0
            but = randint(1, 3)
            if but == 3:
                points3 += 10
            elif but == 1:
                points4 += 10
            vifdor = randint(1, 50)
            if vifdor == 50:
                points3 += 150
                fin = True
            elif vifdor == 1:
                points4 += 150
                fin = True
            scores[equipe3] += points3
            scores[equipe4] += points4
            temps += 5
        resultats2 = tuple(scores)
        
        if resultats2[0] > resultats2[1]:
            vic_3 = True
            vic_4 = False
        else:
            vic_4 = True
            vic_3 = False
        
    partie1()
    partie2()



deux_matchs()

if vic_1:
    victoires_equipe1 +=1
if vic_2:
    victoires_equipe2 +=1
if vic_3:
    victoires_equipe3 +=1
if vic_4:
    victoires_equipe4 +=1

